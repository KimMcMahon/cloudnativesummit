require 'json'
require 'jsonpath'

module Event
    def speakers (featured = false)
        jsonData = JSON.parse(File.read('data/talks.json'))
        featured_data = featured === true ? JsonPath.new('$..speakers[?(@.featured==true)]').on(jsonData) : JsonPath.new('$..speakers[?(@.name)]').on(jsonData)
        speakers_data = ""
        featured_data.each do |item|
            speakers_data += "<div class='col-12 col-sm-6 col-md-4 mt-2 mb-2'><div class='card'><img src=' #{item['image_url']} ' class='card-img-top' alt='...'><div class='card-body'><h5 class='card-title'> #{item['name']} </h5><p class='card-text'> <em> #{item['affiliation'] } </em></p></div></div></div>"
        end

        speakers_data
    end

    def supporters (organizer = false)

        jsonData = JSON.parse(File.read('data/event.json'))
        supportersData = organizer === true ? JsonPath.new('$..supporters[?(@.organizer==true)]').on(jsonData) : JsonPath.new('$..supporters[?(@.organizer==false)]').on(jsonData)

        supporters_data_html = ""
        supportersData.each do |item|
            supporters_data_html += "<div class='col-6 col-md-2 col-sm-4'>
            <a href=' #{item['website']} ' target='_blank'><img src='#{item['logo_url']}' width='100%' class='img-fluid' alt='#{item['name']}'></a></div>"
        end

        supporters_data_html
    end
  end